﻿using System;
using System.Text;
using System.Runtime.InteropServices;

namespace Sajet
{
    public class Sajet
    {
       
        [DllImport("SajetConnect.dll", EntryPoint = "SajetTransData", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern bool SajetTransData(int f_iCommandNo, StringBuilder f_pData, ref int f_pLen);

        [DllImport("SajetConnect.dll", EntryPoint = "SajetTransStart", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern bool SajetTransStart();

        [DllImport("SajetConnect.dll", EntryPoint = "SajetTransClose", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern bool SajetTransClose();


    }
}
